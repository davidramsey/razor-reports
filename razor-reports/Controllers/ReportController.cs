﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using razor_reports.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Net.Mime;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Net.Http.Headers;
using Newtonsoft.Json;
using RazorLight;
using AsyncHelpers;
using MediaTypeHeaderValue = System.Net.Http.Headers.MediaTypeHeaderValue;

namespace razor_reports.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ReportController : Controller
    {


        [HttpGet("html")]
        //public ActionResult<IEnumerable<string>> Get()
        public IActionResult Get()
        {
            ConvertFromHtml converter = new ConvertFromHtml();

            // TODO:  Get the template to be used.  The template needs to be passed in.
            string templateSource = @"/app/template.html";
            // TODO:  Get the data for the template.
            string dataSource = @"/app/data.json";
            // TODO:  use razor light to generate the report in html.
            string report = GetReport(templateSource, dataSource);
            
            // TODO:  Resulting report in html for development.
            //string report = @"<html><head><link href = 'https://fonts.googleapis.com/css?family=Roboto' rel='stylesheet'><link rel = 'stylesheet' href='https://fonts.googleapis.com/icon?family=Material+Icons'><link rel = 'stylesheet' href='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/css/materialize.min.css'><link rel = 'stylesheet' href='css/style.css'> <script src = 'https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js' ></script><script src='https://cdnjs.cloudflare.com/ajax/libs/materialize/0.100.2/js/materialize.min.js'></script> </head><body> <nav role = 'navigation' class='light-blue lighten-1'><div class='nav-wrapper container'><a id = 'logo-container' href='#' class='brand-logo white-text'>Logo</a><ul class='right hide-on-med-and-down'></ul><ul id = 'nav-mobile' class='side-nav light-blue'></ul><a href = '#' data-activates='nav-mobile' class='button-collapse white-text'><i class='material-icons'>menu</i></a></div> </nav> <script type = 'text/javascript' >$('.button-collapse').sideNav();</script> <table class='responsive-table centered striped highlight bordered'><thead><tr><th>Item Name</th><th>Item Price</th></tr></thead><tbody><tr><td> Eclair</td><td> 0.87</td></tr><tr><td> Jellybean</td><td> 03,76</td></tr><tr><td> Lollipop</td><td> 7,00</td></tr><tr><td> CandyBar</td><td> 1.54</td></tr><tr><td> Popcorn</td><td> 3.75</td></tr></tbody></table> <footer class='page-footer light-blue gram-footer'><div class='container'><div class='row'><div class='col l6 s12'><h5 class='white-text'>Company Bio</h5><p class='grey-text text-lighten-4'>We are a team of college students working on this project like its our full time job.Any amount would help support and continue development on this project and is greatly appreciated.</p></div><div class='col l3 s12'><h5 class='white-text'>Settings</h5><ul class='collection light-blue'><li class='collection-item'><a href = '#!' class='white-text'>Link 1</a></li><li class='collection-item'><a href = '#!' class='white-text'>Link 2</a></li><li class='collection-item'><a href = '#!' class='white-text'>Link 3</a></li><li class='collection-item'><a href = '#!' class='white-text'>Link 4</a></li></ul></div><div class='col l3 s12'><h5 class='white-text'>Connect</h5><ul class='collection light-blue with-header'><li class='collection-item'><a href = '#!' class='white-text'>Link 1</a></li><li class='collection-item'><a href = '#!' class='white-text'>Link 2</a></li><li class='collection-item'><a href = '#!' class='white-text'>Link 3</a></li><li class='collection-item'><a href = '#!' class='white-text'>Link 4</a></li></ul></div></div></div><div class='footer-copyright'> dave ramsey</div> </footer></body></html>";

            // unique document id to be passed to the conversion service.
            Guid reportGuid = Guid.NewGuid();

            byte[] pdf = converter.ConvertToPdf(report);

            //return new string[] { "value1", "value2" };
            var contentResult = new ContentResult()
            {
                Content = report,
                ContentType = "text/html"
            };

            return contentResult;
        }

        private string GetReport(string templateSource, string dataSource)
        {
            //return new string[] { "value1", "value2" };
            var engine = new RazorLightEngineBuilder()
                .UseMemoryCachingProvider()
                .Build();

            // TODO:  Get the template.
            string template = System.IO.File.ReadAllText(templateSource);

            // TODO:  Get the data for the template.
            string data = System.IO.File.ReadAllText(dataSource);

            // TODO:  Create the model that will be used by the template.
            var model = JsonConvert.DeserializeObject<ViewModel>(data);

            // Generate the resulting html view from the template and the model.
            var result = AsyncHelper.RunSync(() => engine.CompileRenderAsync("templateKey", template, model));

            return result;
        }


        ////[HttpGet("getHtml")]
        //////public ActionResult<IEnumerable<string>> Get()
        ////public IActionResult GetHtml()
        ////{
        ////    string templateSource = @"/app/template.html";
        ////    string dataSource = @"/app/data.json";

        ////    string report = GetReport(templateSource, dataSource);

        ////    var contentResult = new ContentResult()
        ////    {
        ////        Content = report,
        ////        ContentType = "text/html"
        ////    };

        ////    return contentResult;
        ////}

        ////// GET api/values
        ////[HttpGet("getPdf")]
        //////public ActionResult<IEnumerable<string>> Get()
        ////public IActionResult GetPdf()
        ////{
        ////    string templateSource = @"/app/template.html";
        ////    string dataSource = @"/app/data.json";

        ////    string report = GetReport(templateSource, dataSource);

        ////    // TODO:  Send to converter to get pdf.

        ////    var contentResult = new ContentResult()
        ////    {
        ////        Content = report,
        ////        ContentType = "text/html"
        ////    };

        ////    return contentResult;
        ////}


    }


    public class ViewModel
    {
        public List<Item> Items { get; set; }
    }

    public class Item
    {
        public string ItemName { get; set; }
        public string ItemPrice { get; set; }
    }
}