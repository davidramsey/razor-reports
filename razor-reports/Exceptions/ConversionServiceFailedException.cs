﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace razor_reports.Exceptions
{
    public class ConversionServiceFailedException : Exception
    {
        public ConversionServiceFailedException(int exitCode) : base(string.Format("The Conversion has failed with {}", exitCode))
        {
        }
    }
}
