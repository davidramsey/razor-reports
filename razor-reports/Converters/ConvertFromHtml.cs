﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Threading.Tasks;

namespace razor_reports.Converters
{
    public class ConvertFromHtml
    {
        string workingFolderPath = @"/app/working/";
        
        public byte[] ConvertToPdf(string report)
        {
            Guid documentGuid = Guid.NewGuid();
            string fileName = documentGuid + ".html";
            string convertedFileName = documentGuid + ".pdf";
            string applicationPath = GetApplicationPath();

            // Write the html report to the working folder in the docker container.
            System.IO.File.WriteAllText(Path.Combine(workingFolderPath, fileName), report);
            


            // TODO:  Rewrite to follow:: https://github.com/Reflexe/doc_to_pdf/blob/master/main.cs


            // TODO:  Move to a separate method.
            // Start a process and execute the command to make the conversion.
            ProcessStartInfo processStartInfo = new ProcessStartInfo(
                (applicationPath),
                (string.Format("--convert-to pdf --nologo {0}",
                Path.Combine(workingFolderPath, fileName))));

            processStartInfo.RedirectStandardOutput = true;
            processStartInfo.UseShellExecute = false;
            processStartInfo.CreateNoWindow = true;
            processStartInfo.WorkingDirectory = workingFolderPath;

            Process process = new Process() { StartInfo = processStartInfo, };

            process.Start();
            process.WaitForExit();

            // TODO:  retrieve the generated pdf from its file location (using
            //  its guid) and return to the caller.
            var convertedReport = System.IO.File.ReadAllBytes(Path.Combine(workingFolderPath, convertedFileName));

            // TODO:  Also, need to delete the pdf once it has been retrieved.


            return convertedReport;
        }

        private string GetApplicationPath()
        {
            string applicationPath = string.Empty;
            switch (Environment.OSVersion.Platform)
            {
                case PlatformID.Win32NT:
                    string binaryDirectory = System.IO.Path.GetDirectoryName(Assembly.GetExecutingAssembly().Location);
                    return binaryDirectory + "\\Windows\\program\\soffice.exe";
                case PlatformID.Unix:
                    return "/usr/bin/soffice";
                default:
                    throw new PlatformNotSupportedException(Environment.OSVersion.Platform.ToString() + " is not supported.");
            }
        }
    }
}
